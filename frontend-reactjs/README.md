# React Typescript Login example with Axios and Web API

Build React Typescript Login and Registration example with React Router, Axios and Bootstrap (without Redux):
- JWT Authentication Flow for User Signup & User Login
- Project Structure for React Typescript Authentication (without Redux) with React Router & Axios
- Creating React Components with Form Validation using Formik and Yup
- React Typescript Components for accessing protected Resources (Authorization)
- Dynamic Navigation Bar in React Typescript App

Signup Page:

![react-typescript-login-example-signup](react-typescript-login-example-signup.png)

Form Validation:

![react-typescript-login-example-form-validation](react-typescript-login-example-form-validation.png)

Login Page:

![react-typescript-login-example-login](react-typescript-login-example-login.png)

### Set port
.env
```
PORT=8081
```

## Project setup
In the project directory, you can run:
```
npm install
# or
yarn install
```
or

### Compiles and hot-reloads for development
```
npm start
# or
yarn start
```

Open [http://localhost:8081](http://localhost:8081) to view it in the browser.
The page will reload if you make edits.
